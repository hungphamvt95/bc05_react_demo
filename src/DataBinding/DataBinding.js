import React, { Component } from "react";
export default class DataBinding extends Component {
  // nam ngoai ham render nen tuong duong doi tuong nen goi "this."
  imgSrc = "https://movienew.cybersoft.edu.vn/hinhanh/home.jpg";
  renderDescription = () => {
    return <div>Hello to Alice</div>;
  };
  render() {
    //trong ham render thi phai dung let
    let username = "Alice";
    return (
      <div>
        <div>
          <div
            style={{ width: "200px", height: "500px", backgroundColor: "red" }}
            classname="card text-left"
          >
            <img classname="card-img-top" src={this.imgSrc} alt />
            <div classname="card-body">
              <h4 classname="card-title">{username}</h4>
              <p classname="card-text">{this.renderDescription()}</p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
