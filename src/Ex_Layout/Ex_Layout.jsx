import React, { Component } from "react";
import { Header } from "./Header";
import { Navigate } from "./Navigate";
import { Content } from "./Content";
import { Footer } from "./Footer";
export default class Ex_Layout extends Component {
  render() {
    return (
      <div className="container text-white">
        Ex_Layout
        <Header></Header>
        <div className="row mx-0">
          <div className="col-6 p-0">
            <Navigate></Navigate>
          </div>
          <div className="col-6 p-0">
            <Content></Content>
          </div>
        </div>
        <Footer></Footer>
      </div>
    );
  }
}
