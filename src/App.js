import logo from "./logo.svg";
import "./App.css";
// import DemoClass from "./DemoComponent/DemoClass.jsx";
// import DemoFunction from "./DemoComponent/DemoFunction.jsx";
// import Ex_Layout from "./Ex_Layout/Ex_Layout.jsx";
// import DataBinding from "./DataBinding/DataBinding";
// import ConditionalRendering from "./ConditionalRendering/ConditionalRendering";
// import Ex_Tai_Xiu from "./Ex_Tai_Xiu/Ex_Tai_Xiu";
// import LifeCycle from "./LifeCycle/LifeCycle";
// import Ex_ShoeShop_Redux from "./Ex_ShoeShop_Redux/Ex_ShoeShop_Redux";
// import DemoProps from "./DemoProps/DemoProps";
// import RenderWithMap from "./RenderWithMap/RenderWithMap";
// import DemoState from "./DemoState/DemoState";
// import Ex_Car from "./Ex_Car/Ex_Car";
// import Ex_ShoeShop from "./Ex_ShoeShop/Ex_ShoeShop";
// import Demo_Mini_Redux from "./Demo_Mini_Redux/Demo_Mini_Redux";
import Ex_Form from "./Ex_Form/Ex_Form";
function App() {
  return (
    <div className="App">
      <div className="App">
        {/* <DemoClass />
        <DemoFunction></DemoFunction>
        <DemoFunction></DemoFunction> */}
        {/* <Ex_Layout /> */}
        {/* <DataBinding /> */}
        {/* <RenderWithMap/> */}
        {/* <DemoProps /> */}
        {/* <RenderWithMap /> */}
        {/* <DemoState /> */}
        {/* <Ex_Car /> */}
        {/* <ConditionalRendering /> */}
        {/* <Ex_ShoeShop /> */}
        {/* <Demo_Mini_Redux /> */}
        {/* <Ex_ShoeShop_Redux /> */}
        {/* <Ex_Tai_Xiu /> */}
        {/* <LifeCycle /> */}
        <Ex_Form />
      </div>
    </div>
  );
}

export default App;
