import { CHOOSE_OPTION, PLAY_GAME } from "./xucXacConstant";

let initialState = {
  mangXucXac: [
    {
      img: "./imgXucSac/1.png",
      giaTri: 1,
    },
    {
      img: "./imgXucSac/1.png",
      giaTri: 1,
    },
    {
      img: "./imgXucSac/1.png",
      giaTri: 1,
    },
  ],
  luaChon: null,
  soLuotThang: 0,
  soLuotChoi: 0,
};
export const XucXacReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case PLAY_GAME: {
      console.log("yes");
      let newMangXucXac = state.mangXucXac.map((item) => {
        let random = Math.floor(Math.random() * 6) + 1;
        return {
          img: `./imgXucSac/${random}.png`,
          giaTri: random,
        };
      });
      state.mangXucXac = newMangXucXac;
      return { ...state };
    }
    case CHOOSE_OPTION: {
      return {
        ...state,
        luaChon: payload,
      };
    }
    default:
      return state;
  }
};
