import React, { Component } from "react";

export default class CardItem extends Component {
  render() {
    console.log(this.props);
    return (
      <div className="card text-left h-100">
        <img
          style={{ height: "80%", objectFit: "cover" }}
          className="card-img-top"
          src={this.props.movie.hinhAnh}
          alt
        />
        <div className="card-body">
          <h4 className="card-title">{this.props.movie.tenPhim}</h4>
        </div>
      </div>
    );
  }
}
