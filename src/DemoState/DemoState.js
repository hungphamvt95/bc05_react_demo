import React, { Component } from "react";

export default class DemoState extends Component {
  state = {
    like: 0,
  };
  handlePlusLike = () => {
    // this.state.like = 10;
    // set state là bất đồng bộ nên dòng console log phía dưới sẽ chạy trước
    this.setState(
      {
        like: this.state.like + 1, // không được dùng this.state.like ++
      },
      () => {
        console.log("thành công ", this.state.like);
      }
    );
    // console.log("this.state.like: ", this.state.like);
  };
  render() {
    console.log("this.state.like: ", this.state.like);
    return (
      <div>
        <p>{this.state.like}</p>
        <button onClick={this.handlePlusLike} className="btn btn-success">
          Plus like
        </button>
      </div>
    );
  }
}
