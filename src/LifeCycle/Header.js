import React, { PureComponent } from "react";

export default class Header extends PureComponent {
  // PureComponent để không render lại Header
  componentDidMount() {
    let time = 300;
    this.myCountDowwn = setInterval(() => {
      console.log("count down", time--);
    }, 1000);
  }
  render() {
    console.log("HEADER render");
    return <div className="p-5 bg-dark text-white">Header</div>;
  }
  componentWillUnmount() {
    console.log("HEADER componentWillUnmount");
    clearInterval(this.myCountDowwn);
  }
}
