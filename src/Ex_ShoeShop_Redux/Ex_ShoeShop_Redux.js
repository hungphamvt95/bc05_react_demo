import React, { Component } from "react";
import { dataShoe } from "./dataShoe";
import ListShoe from "./ListShoe";
import DetailShoe from "./DetailShoe";
import Cart from "./Cart";

export default class Ex_ShoeShop_Redux extends Component {
  state = {
    shoeArr: [],
    detail: dataShoe[0],
    cart: [],
  };

  render() {
    return (
      <div className="container">
        <Cart />
        <ListShoe />
        <DetailShoe />
      </div>
    );
  }
}
