import React, { Component } from "react";
import { dataShoe } from "./dataShoe";
import ListShoe from "./ListShoe";
import DetailShoe from "./DetailShoe";
import Cart from "./Cart";

export default class Ex_ShoeShop extends Component {
  state = {
    shoeArr: dataShoe,
    detail: dataShoe[0],
    cart: [],
  };
  handleChangeDetail = (value) => {
    this.setState({ detail: value });
  };
  handleAddToCart = (shoe) => {
    // let cloneCart = [...this.state.cart]; //tạo ra phiên bản copy
    // cloneCart.push(shoe);
    // this.setState({ cart: cloneCart });
    let cloneCart = [...this.state.cart];
    let index = this.state.cart.findIndex((item) => {
      return item.id == shoe.id;
    });

    if (index == -1) {
      //th1: sản phẩm chưa có trong giỏ hàng
      let cartItem = { ...shoe, number: 1 };
      cloneCart.push(cartItem);
    } else {
      cloneCart[index].number++;
    }

    this.setState({
      cart: cloneCart,
    });
  };
  render() {
    return (
      <div className="container">
        <Cart cart={this.state.cart} />
        <ListShoe
          handleAddToCart={this.handleAddToCart}
          handleChangeDetail={this.handleChangeDetail}
          shoeArr={this.state.shoeArr}
        />
        <DetailShoe detail={this.state.detail} />
      </div>
    );
  }
}
