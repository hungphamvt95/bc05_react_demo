import React, { Component } from "react";
import UserForm from "./UserForm";
import UserTable from "./UserTable";

export default class Ex_Form extends Component {
  state = {
    userList: [
      {
        id: 1,
        name: "Alice",
        password: "1234",
      },
    ],
    userEdited: null,
  };
  handleUserAdd = (newUser) => {
    let cloneUserList = [...this.state.userList, newUser];
    this.setState({ userList: cloneUserList });
  };
  handleUserRemove = (userId) => {
    let index = this.state.userList.findIndex((item) => {
      return item.id == userId;
    });
    if (index != -1) {
      let cloneUserList = [...this.state.userList];
      cloneUserList.splice(index, 1);
      this.setState({ userList: cloneUserList });
    }
  };
  handleUserEdit = (value) => {
    this.setState({ userEdited: value });
  };
  render() {
    return (
      <div className="container py-5">
        <UserForm
          handleUserAdd={this.handleUserAdd}
          userEdited={this.state.userEdited}
        />
        <UserTable
          userList={this.state.userList}
          handleUserRemove={this.handleUserRemove}
          handleUserEdit={this.handleUserEdit}
        />
      </div>
    );
  }
}
